#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_include_static_resources;
extern crate crypto;
extern crate rocket_contrib;
extern crate simplebase;
extern crate webbrowser;

mod first;

use rocket::request::Form;
use rocket::response::content;
use rocket::response::Redirect;
use rocket_include_static_resources::StaticResponse;
use serde::{Deserialize, Serialize};
use simplebase::engine::*;
use std::path::PathBuf;

#[macro_export]
macro_rules! hash {
    ($value:expr) => {{
        use crypto::digest::Digest;
        use crypto::sha3::Sha3;
        let mut h = Sha3::sha3_512();
        h.input_str(&$value[..]);
        h.result_str()
    }};
}

#[derive(FromForm, Debug, Serialize, Deserialize)]
struct Index {
    name: String,
    amount1: u64,
    address1: String,
    amount2: u64,
    address2: String,
}

/// Renders the Homepage template.
#[get("/")]
fn index() -> StaticResponse {
    static_response!("index.html")
}

#[post("/index_post", data = "<data>")]
fn index_post(data: Form<Index>) -> content::Html<String> {
    // let mut database = load_hash_database("data.txt");
    // let value = serde_json::to_string(&(data.into())).unwrap();
    // database.add_record(value);
    content::Html(format!("
<!doctype html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <script src=\"index.js\"></script>
    <link rel=\"icon\" href=\"/docs/4.0/assets/img/favicons/favicon.ico\">
    <title>THE OASIS</title>
    <link rel=\"canonical\" href=\"https://getbootstrap.com/docs/4.0/examples/starter-template/\">
    <!-- Bootstrap core CSS -->
    <link href=\"/static/css/style.css\" rel=\"stylesheet\">
    <!-- Custom styles for this template -->
    <link href=\"style.css\" rel=\"stylesheet\">
</head>

<body>
    <nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">
        <a class=\"navbar-brand\" href=\"#\">
            <img src=\"/static/images/oasis.jpg\" style=\"height: 90px\"></img>
        </a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item active\">
                    <a class=\"nav-link\" href=\"#\">Axie Bets <span class=\"sr-only\">(current)</span></a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"#\">Current Games </a>
                </li>
                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"dropdown01\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Choose Game</a>
                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdown01\">
                        <a class=\"dropdown-item\" href=\"#\">Action</a>
                        <a class=\"dropdown-item\" href=\"#\">Another action</a>
                        <a class=\"dropdown-item\" href=\"#\">Something else here</a>
                    </div>
                </li>
            </ul>
            <form class=\"form-inline my-2 my-lg-0\">
                <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\" aria-label=\"Search\">
                <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"submit\">Search</button>
            </form>
        </div>
    </nav>
    <main role=\"main\" class=\"container\">
        <div class=\"starter-template\">
            <h1>Place your bets!</h1>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm\" style=\"font-size: 120px;\">
                        <div id=\"A\" class=\"card bg-danger text-white\">
                            <div class=\"card-body\">A </div>
                        </div>
                    </div>
                    <div class=\"col-sm\" style=\"font-size: 120px;\">
                        <div id=\"B\" class=\"card bg-primary text-white\">
                            <div class=\"card-body\">B </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class=\"lead\">Click on who you wish to place your bets on!</p>
            <div class=\"card bg-primary text-white\">
                <div class=\"card-body\">{}<br>{}<br>{}<br>{}<br>{}<br></div>
            </div>
            <form action=\"/action_page.php\">
                <br>
                <input type=\"text\" name=\"name\" placeholder=\"Enter name\">
                <input type=\"text\" name=\"amount\" placeholder=\"Enter amount\">
                <input type=\"text\" name=\"address\" placeholder=\"Enter address\">
                <br><br>
                <input type=\"submit\" value=\"Submit\">
            </form>
        </div>
    </main><!-- /.container -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
    <script>
    window.jQuery || document.write('<script src=\"../../assets/js/vendor/jquery-slim.min.js\"></script>')
    </script>
    <script src=\"../../assets/js/vendor/popper.min.js\"></script>
    <script src=\"../../dist/js/bootstrap.min.js\"></script>
</body>

</html>
        ", data.name, data.amount1, data.address1, data.amount2, data.address2))
}

// /// Handles the button input of the Homepage.
// #[post("/homepage_form", data = "<data>")]
// fn homepage_form(data: Form<Homepage>) -> Redirect {
//     match &data.button[..] {
//         "first" => Redirect::to("/book"),
//         "second" => Redirect::to("/issue"),
//         _ => panic!("Something is wrong in \"homepage_form\"..."),
//     }
// }

// /// Renders the Input Books template.
// #[get("/book")]
// fn book() -> StaticResponse {
//     static_response!("book.html")
// }

// /// Handles the form of the Input Books.
// #[post("/book_form", data = "<data>")]
// fn book_form(data: Form<Book>) -> Redirect {
//     match &data.button[..] {
//         "submit" => {
//             book::input(
//                 data.name.clone(),
//                 data.author.clone(),
//                 data.book_code.clone(),
//             )
//             .unwrap();
//             Redirect::to("/book")
//         }
//         "back" => Redirect::to("/"),
//         _ => panic!("Something is wrong in \"book_form\"..."),
//     }
// }

// /// Renders the Book Issue template.
// #[get("/issue")]
// fn issue() -> StaticResponse {
//     static_response!("issue.html")
// }

// /// Handles the form of Book Issue.
// #[post("/issue_form", data = "<data>")]
// fn issue_form(data: Form<Issue>) -> Redirect {
//     println!("{:?}", data);
//     match &data.button[..] {
//         "submit" => {
//             issue::issue(
//                 data.name.clone(),
//                 data.class_name.clone(),
//                 data.id.clone(),
//                 data.book_code.clone(),
//             )
//             .unwrap();
//             Redirect::to("/issue")
//         }
//         "back" => Redirect::to("/"),
//         _ => panic!("Something is wrong in \"issue_form\"..."),
//     }
// }

fn main() {
    first::init();
    rocket::ignite()
        .attach(StaticResponse::fairing(|resources| {
            static_resources_initialize!(
                resources,
                "index.html",
                "templates/index.html",
                "static/css/style.css",
                "static/css/style.css",
                "static/images/oasis.jpg",
                "static/images/oasis.jpg"
            );
        }))
        .mount("/", routes![file])
        .mount("/", routes![index, index_post])
        .launch();
}

#[get("/<file..>")]
fn file(file: PathBuf) -> StaticResponse {
    static_response!(Box::leak(
        file.to_str().unwrap().to_string().into_boxed_str()
    ))
}
