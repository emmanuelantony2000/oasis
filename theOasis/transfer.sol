pragma solidity >=0.4.22 <0.6.0;
import "./coin.sol";

contract Transfer is Coin {
    struct Bet {
        uint a;
        uint b;
        address a_address;
        address b_address;
    }
    
    Bet[] bets;

    constructor(uint[] memory a_s, uint[] memory b_s, address[] memory a_address_s, address[] memory b_address_s) public {
        for (uint i = 0; i < a_s.length; i++) {
            mint(a_address_s[i], 50);
            mint(b_address_s[i], 50);
            bets.push(Bet({
                a: a_s[i],
                b: b_s[i],
                a_address: a_address_s[i],
                b_address: b_address_s[i]
            }));
        }
    }
    
    function eval(bool r) public {
        if (r) {
            for (uint i = 0; i < bets.length; i++) {
                send(bets[i].b_address, bets[i].a_address, bets[i].b);
            }
        } else {
            for (uint i = 0; i < bets.length; i++) {
                send(bets[i].a_address, bets[i].b_address, bets[i].a);
            }
        }
    }
}

// contract Transfer {
//     struct Bet {
//         uint a;
//         uint b;
//         address a_address;
//         address b_address;
//     }
    
//     Bet[] bets;
//     Coin c;

//     constructor(uint[] memory a_s, uint[] memory b_s, address[] memory a_address_s, address[] memory b_address_s) public {
//         c = new Coin();
//         for (uint i = 0; i < a_s.length; i++) {
//             c.mint(a_address_s[i], 50);
//             c.mint(b_address_s[i], 50);
//             bets.push(Bet({
//                 a: a_s[i],
//                 b: b_s[i],
//                 a_address: a_address_s[i],
//                 b_address: b_address_s[i]
//             }));
//         }
//     }
    
//     function eval(bool r) public {
//         if (r) {
//             for (uint i = 0; i < bets.length; i++) {
//                 c.send(bets[i].b_address, bets[i].a_address, bets[i].b);
//             }
//         } else {
//             for (uint i = 0; i < bets.length; i++) {
//                 c.send(bets[i].a_address, bets[i].b_address, bets[i].a);
//             }
//         }
//     }
// }

// contract Transfer {
//     struct Bet {
//         uint a;
//         uint b;
//         address a_address;
//         address b_address;
//         Coin a_coin;
//         Coin b_coin;
//     }
    
//     Bet[] bets;

//     constructor(uint[] memory a_s, uint[] memory b_s, address[] memory a_address_s, address[] memory b_address_s) public {
//         for (uint i = 0; i < a_s.length; i++) {
//             coin_a = Coin(a_address_s[i]);
//             coin_b = Coin(b_address_s[i]);
//             coin_a.mint(a_address_s[i], 50);
//             coin_b.mint(b_address_s[i], 50);
//             bets.push(Bet({
//                 a: a_s[i],
//                 b: b_s[i],
//                 a_address: a_address_s[i],
//                 b_address: b_address_s[i],
//                 a_coin: coin_a,
//                 b_coin: coin_b
//             }));
//             // init(i);
//         }
//     }
    
//     // function init(uint i) public {
//     //     bets[i].a_coin.mint(bet.a_address, 50);
//     //     bets[i].b_coin.mint(bet.b_address, 50);
//     // }
    
//     function eval(bool r) public {
//         if (r) {
//             for (uint i = 0; i < bets.length; i++) {
//                 bets[i].b_coin.send(bets[i].a_address, bets[i].b);
//             }
//         } else {
//             for (uint i = 0; i < bets.length; i++) {
//                 bets[i].a_coin.send(bets[i].b_address, bets[i].a);
//             }
//         }
//     }
// }
