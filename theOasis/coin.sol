pragma solidity >=0.4.22 <0.6.0;

contract Coin {
    mapping (address => uint) public balances;
    
    event Sent(address from, address to, uint amount);

    function mint(address account, uint amount) public {
        require(amount < 1e60);
        balances[account] += amount;
    }

    function send(address sender, address receiver, uint amount) public {
        require(amount <= balances[sender], "Insufficient balance.");
        balances[sender] -= amount;
        balances[receiver] += amount;
        emit Sent(sender, receiver, amount);
    }
}

// contract Coin {
//     address public minter;
//     mapping (address => uint) public balances;
    
//     event Sent(address from, address to, uint amount);
    
//     constructor() public {
//         minter = msg.sender;
//     }

//     function mint(address receiver, uint amount) public {
//         require(msg.sender == minter);
//         require(amount < 1e60);
//         balances[receiver] += amount;
//     }

//     function send(address receiver, uint amount) public {
//         require(amount <= balances[msg.sender], "Insufficient balance.");
//         // if (amount <= balances[msg.sender]) return false;
//         balances[msg.sender] -= amount;
//         balances[receiver] += amount;
//         // return true;
//         emit Sent(msg.sender, receiver, amount);
//     }
// }
